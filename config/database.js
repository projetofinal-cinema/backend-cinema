const { Sequelize } = require("sequelize");

const sequelize = new Sequelize({
    "dialect": "sqlite",
    "storage": "./data/database.sqlite"
});

async function testConection(){
    try {
        await sequelize.authenticate();
    } catch (error) {
        console.log(('Nao foi possivel se conectar ao banco de dados.'))
        console.log(error)
    }
}

testConection();

module.exports = sequelize;