const { DataTypes } = require("sequelize");

const sequelize = require('../../config/database');
const { v4: uuidv4 } = require('uuid');


const Filme = sequelize.define('Filme', {
    idFilme: {
        type: DataTypes.UUID,

        defaultValue: ()=> uuidv4(),
        primaryKey: true,
        allowNull: false
    },
    titulo: {
        type: DataTypes.STRING,
    },
    urlImagem: {
        type: DataTypes.STRING,
    },
    sinopse: {
        type: DataTypes.STRING
    },
    genero: {
        type: DataTypes.STRING,
    },
    classificacao: {
        type: DataTypes.INTEGER,
    },
    diretor: {
        type: DataTypes.STRING,
    },
    
});

const Sessao = sequelize.define('Sessao', {
    idSessao: {
        type: DataTypes.UUID,
        defaultValue: ()=> uuidv4(),
        primaryKey: true,
        allowNull: false
    },
    horario: {
        type: DataTypes.STRING,
    },
    cidade: {
        type: DataTypes.STRING,
    },
    bairro: {
        type: DataTypes.STRING,
    },
    tipo: {
        type: DataTypes.INTEGER,
    },
    idFilme: {
        type: DataTypes.UUID
    }
} );

const Assento = sequelize.define('Assento', {
    idAssento: {
        type: DataTypes.UUID,
        defaultValue: ()=> uuidv4(),
        primaryKey: true,
        allowNull: false
    },
    numero: {
        type: DataTypes.INTEGER,
    },
    fileira : {
        type: DataTypes.STRING 
    },
    preco: {
        type: DataTypes.FLOAT,
    },
    cpfOcupante: {
        type: DataTypes.STRING,
    },
    nomeOcupante: {
        type: DataTypes.STRING,
    }  
} );

const Usuario = sequelize.define('Usuario', {
    idUsuario: {
        type: DataTypes.UUID,
        defaultValue: ()=> uuidv4(),
        primaryKey: true,
        allowNull: false
    },
    nome: {
        type: DataTypes.STRING,
    },
    sobrenome: {
        type: DataTypes.STRING,
    },
    cpf: {
        type: DataTypes.STRING,
    },
    dataDeNascimento: {
        type: DataTypes.STRING,
    },
    nomeDeUsuario: {
        type: DataTypes.STRING,
    },
    email: {
        type: DataTypes.STRING,
    },
    senha: {
        type: DataTypes.STRING,
    }
     
} );

Filme.hasMany(Sessao, { onDelete: 'CASCADE', foreignKey: "idFilme"});
Sessao.belongsTo(Filme, {foreignKey: "idFilme"});

Sessao.hasMany(Assento, { onDelete: 'CASCADE' });
Assento.belongsTo(Sessao);

Assento.belongsTo(Usuario);
Usuario.hasMany(Assento, { onDelete: 'CASCADE' });

module.exports = {
    Filme,
    Sessao,
    Assento,
    Usuario
}

