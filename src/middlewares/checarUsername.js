const { Usuario } = require("../models/models")

async function checarUsername(request, response, next){

    try{
        const { nomeDeUsuario } = request.body;
        const usuario = await Usuario.findOne({
            where: {
                nomeDeUsuario,
            },
        });
        
        if (usuario){
            return response.status(400).json({error: "Este nome ja esta sendo usado!"});
        }
    
        return next();

    }catch(error){
        return response.status(500).json({error:'Erro ao checar se o username.'})
    }
}
module.exports = checarUsername;
