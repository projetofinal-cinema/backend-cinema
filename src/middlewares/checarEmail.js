const { Usuario } = require("../models/models")

async function checarEmail(request, response, next){

    try{
        const { email } = request.body;
        const usuario = await Usuario.findOne({
            where: {
                email,
            },
        });
        
        if (usuario){
            return response.status(400).json({error: "Este email ja esta cadastrado!"});
        }
    
        return next();

    }catch(error){
        return response.status(500).json({error:'Erro ao checar se o email.'})
    }
}
module.exports = checarEmail;
