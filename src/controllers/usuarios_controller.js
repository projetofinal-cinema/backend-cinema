const { Usuario } = require("../models/models");
const { Op } = require('sequelize')
const bcrypt = require("bcrypt")

async function loginUser(req, res) {
    try {
        const { email, nomeDeUsuario, senha } = req.body;
        const user = await Usuario.findOne({
            where: {
                [Op.or]: [
                    { email: email || "" },
                    { nomeDeUsuario: nomeDeUsuario || "" }
                ]
            }
        });
        if (!user) {
            return res.status(400).json({ error: 'Não foi possível encontrar o usuário!' });
        }
        if (!(await bcrypt.compare(senha, user.senha))) {
            return res.status(400).json({ error: 'Senha incorreta!' });
        }

        return res.status(200).json({ message: 'Login efetuado com sucesso' });
    } catch (error) {
        return res.status(500).json({ error: 'Ocorreu um erro ao processar a requisição.' });
    }
}

module.exports = {
    loginUser
}   