const {v4:uuidv4} = require('uuid')

const { Op } = require("sequelize")

const {Filme} = require("../models/models")

async function criarFilme(req,res){
    try{
        console.log(req)
        console.log(req.body)
        const {titulo,urlImagem,sinopse,genero,classificacao,diretor, descricao} = req.body
        console.log(req.body)
        const filme = await Filme.create({
            titulo,
            urlImagem,
            sinopse,
            genero,
            classificacao,
            diretor,
        })

        return res.status(201).json(filme)
    }catch(error){
        console.log(error)
        return  res.status(400).json({error : "nao foi possivel criar o filme"})
    }
}

async function getFilme(req,res){
    try{
        const Filmess = await Filme.findAll()
        return res.status(200).json(Filmess)
        
    }catch(error){
        return  res.status(400).json({error : "nao foi possivel listar os Filmes"})
    }
}

async function getFilmporId(req,res){
    try{
        const {filmeId} = req.params
        let filmee = await Filme.findByPk(filmeId)

        if(!filmee){
            return res.status(404).json({message: "filme nao existe"})
        }

        return res.status(200).json(filmee)
    }catch(error){
        return  res.status(400).json({error : "nao foi possivel encontrar o filme"})
    }
}

async function getFilmesPorGenero(req, res) {
    const { genero } = req.params;

    try {
        const filmes = await Filme.findAll({
            where: {
                genero: genero,
            },
        });
        if (!filmes) {
            return res.status(404).json({ message: "Filme não encontrado." });
        }

        return res.json(filmes);
    } catch (error) {
        return res.status(500).json({ error: "Ocorreu um erro ao buscar os filmes." });
    }
}

async function getFilmesPorCI(req, res) {
    const { classInd } = req.params;

    try {
       
        const filmes = await Filme.findAll({
            where: {
                classificacao: {
                    [Op.eq]: classInd
                },
            },
        });
        
        
        if (!filmes) {
            return res.status(404).json({ message: "Filme não encontrado." });
        }

        return res.json(filmes);
    } catch (error) {
        return res.status(500).json({ error: "Ocorreu um erro ao buscar os filmes." });
    }
}

async function getFilmesPorTitulo(req, res) {
    const { titulo } = req.params;

    try {
        const filmes = await Filme.findAll({
            
            where: {
                titulo: {
                    [Op.like]: `%${titulo}%` 
                }
            }
        });

        return res.json(filmes);
    } catch (error) {
        return res.status(500).json({ error: "Ocorreu um erro ao buscar os filmes." });
    }
}

async function getFilmeAll(req, res) {
    const query = req.query;
    try {
       
        const filmes = await Filme.findAll({
            where: query

        });
        
        
        if (!filmes) {
            return res.status(404).json({ message: "Filme não encontrado." });
        }
        console.log("oi")
        return res.json(filmes);
    } catch (error) {
        return res.status(500).json({ error: "Ocorreu um erro ao buscar os filmes." });
    }
}




module.exports = {
    criarFilme,
    getFilmeAll,
    getFilme,
    getFilmporId,
    getFilmesPorGenero,
    getFilmesPorCI,
    getFilmesPorTitulo
}