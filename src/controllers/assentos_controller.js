const { Assento } = require("../models/models");

async function criarAssento(sessaoId) {
    const rows = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];
    const numAssentosPorFileira = 18;
    const createdAssentos = [];
    console.log(sessaoId)
    for (row of rows) {
        for (let i = 1; i <= numAssentosPorFileira; i++) {
            
            const novoAssento = await Assento.create({
                numero: i,
                fileira: row,
                preco: "",
                cpfOcupante: "",
                nomeOcupante: "",
                sessoesId: sessaoId,
            });
            createdAssentos.push(novoAssento);
        }
    }

    return createdAssentos;
}

async function listaAssentoPorSessao(sessaoId) {
    try {
        const assentosDaSessao = await Assento.findAll({
            where: {
                sessoesId: sessaoId,
            },
        });

        return assentosDaSessao;
    } catch (error) {
        throw new Error("Erro ao listar os assentos da sessão.");
    }
}

async function bookAssento(req, res) {
    const { assentoId, cpfNovo, nomeNovo } = req.body;

    try {
        const assento = await Assento.findByPk(assentoId);

        if (!assento) {
            return res.status(404).json({ message: "Assento não encontrado." });
        }

        
        assento.cpf = cpfNovo;
        assento.nome = nomeNovo;
        await assento.save();

        return res.json({ message: "Assento atualizado com sucesso.", assentoAtualizado: assento });
    } catch (error) {
        return res.status(500).json({ error: "Ocorreu um erro ao atualizar o assento." });
    }
}

module.exports = {
    criarAssento,
    listaAssentoPorSessao,
    bookAssento
    
};