        
const { query } = require("express")
const {Sessao} = require("../models/models")
const {Filme} = require("../models/models")
const assentosController = require("./assentos_controller")

async function criarSessao(req,res){
    try{
        const {idFilme} = req.params
        const {horario,cidade,bairro,tipo} = req.body
        const filme = await Filme.findByPk(idFilme)
        if(!filme){
            return res.json({message: "filme nao encontrado"})
        }
        
        novaSessao = await Sessao.create({
            horario,
            cidade,
            bairro,
            tipo,
            idFilme: idFilme
        })     
        
 
        const createdAssentos = await assentosController.criarAssento(novaSessao.sessoesId);
        
        novaSessao.dataValues.assentos = createdAssentos;

        return res.json(novaSessao)
   
    }catch(error){
        console.log(error)
        return res.json({mess: "errorrr"})
    }
    
}

async function listaAssentosDeUmaSessao(req, res) {
    const { sessaoId } = req.params;

    try {
        const assentosDaSessao = await assentosController.listAssentosBySessao(sessaoId);
        return res.json(assentosDaSessao);
    } catch (error) {
        return res.status(500).json({ error: "Erro ao obter os assentos da sessão." });
    }
}

async function getSessaoPorId(req, res) {
    
    const {sessaoId } = req.params
    try {
        
        const sessao = await Sessao.findByPk(sessaoId)
        
        
        res.status(200).json(sessao)
    } catch (error) {
        console.log(error)
        res.status(500).json(error)
    }
}

async function getSessaoAll(req, res) {
    
    const { idFilme } = req.params
    const query = req.query
    try {
        const sessao = await Sessao.findAll({
            where:{
                ...query,
                idFilme: idFilme
                
            },
           
            
        })
        if(!sessao){
            return res.json({message: "nao foi possivel encontrar as sessoes do filme"})
        }

        res.status(200).json(sessao)
    } catch (error) {
        console.log(error)
        res.status(500).json(error)
    }
}


module.exports = {
    getSessaoAll,
    criarSessao,
    listaAssentosDeUmaSessao,
    getSessaoPorId
}