const express = require('express');
const cors = require("cors");
const createDatabase = require('../scripts/createDatabase');

const app = express();
const port = 3000;
const router = require("./routes")


app.use(express.json())
app.use(cors())

app.get('/', (req,res)=>{
    res.send("Hello world")
});

app.use(router)
app.listen(port,()=>{{
    console.log("Server is running...")

}});




createDatabase();