const Router = require('express');
const router = Router();

const filmesRoute = require("./filmes.routes")
const sessoesRouter = require("./sessoes.routes")
const usuariosRoute = require('./usuarios.routes')
const assentosRouter = require("./assentos.routes")

router.use('/usuario', usuariosRoute);
router.use('/filmes', filmesRoute)
router.use('/sessoes', sessoesRouter)
router.use('/assentos', assentosRouter)


module.exports = router;