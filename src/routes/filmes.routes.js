const express = require("express")
const filmesRoutes = express.Router()
const filmesController = require("../controllers/filmes_controller")

//listagem de todos os filmes
filmesRoutes.get('/lista', async (req, res) => {
    filmesController.getFilme(req, res)
})

//get filme filtrado
filmesRoutes.get('/', async (req, res) => {
    filmesController.getFilmeAll(req, res)
})


//listagem de um filme especifico
filmesRoutes.get('/:idFilme', async (req, res) => {
    filmesController.getFilmporId(req, res)
})

//postagem de um filme
filmesRoutes.post('/', async (req, res) => {
    filmesController.criarFilme(req, res)
})

//get filme por genero
filmesRoutes.get("/genero/:genero", async (req, res) => {
    filmesController.getFilmesPorGenero(req, res)
});

//get filme por classind
filmesRoutes.get("/classInd/:classInd", async (req, res) => {
    filmesController.getFilmesPorCI(req, res)
});

//get filme por titulo
filmesRoutes.get("/search/:titulo", async (req, res) => {
    filmesController.getFilmesPorTitulo(req, res)
});

module.exports = filmesRoutes;