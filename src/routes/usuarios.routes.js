const express = require('express') ;
const usuariosRoute = express.Router();
const usuarios_controller = require("../controllers/usuarios_controller");

//inclusao middlwares
const checarEmail = require('../middlewares/checarEmail');
const checarUsername = require('../middlewares/checarUsername');

usuariosRoute.post('/register',checarEmail,checarUsername,(req,res)=>usuarios_controller.createUser(req,res))
usuariosRoute.post('/login',checarEmail,checarUsername,(req,res)=>usuarios_controller.loginUser(req,res))


module.exports = usuariosRoute;