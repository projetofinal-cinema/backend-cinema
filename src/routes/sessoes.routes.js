const express = require("express")
const sessoesRoutes = express.Router()
const sessoesController = require("../controllers/sessoes_controller")


//listagem das sessoes de um filme


//criar sessao

sessoesRoutes.post('/:idFilme', (req,res) => {
    sessoesController.criarSessao(req,res)
})

//achar sessao especifica
sessoesRoutes.get("/:idFilme/:sessaoId" , (req, res) => {
    sessoesController.getSessaoPorId(req, res)
})


sessoesRoutes.get("/:idFilme" , (req, res) => {
    sessoesController.getSessaoAll(req, res)
})

module.exports = sessoesRoutes