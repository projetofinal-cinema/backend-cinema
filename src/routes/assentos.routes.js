const express = require("express")
const assentosRoutes = express.Router()
const assentosController = require("../controllers/assentos_controller")

//lsitar assentos
assentosRoutes.get("/:sessaoId", async (req, res) => {
    const { sessaoId } = req.params;

    try {
        const assentosDaSessao = await assentosController.listaAssentoPorSessao(sessaoId);
        return res.json(assentosDaSessao);
    } catch (error) {
        return res.status(500).json({ error: "Erro ao obter os assentos da sessão." });
    }
});



//criar assentos

assentosRoutes.post("/criar/:sessaoId/:numAssentos", async (req, res) => {
    const { sessaoId, numAssentos } = req.params;

    try {
        const createdAssentos = await assentosController.criarAssento(sessaoId, parseInt(numAssentos));
        return res.json(createdAssentos);
    } catch (error) {
        return res.status(500).json({ error: "Erro ao criar os assentos." });
    }
});


//book assento
assentosRoutes.patch("/book/:assentoId", async (req, res) => {
    assentosController.bookAssento(req, res)
    });

module.exports = assentosRoutes;