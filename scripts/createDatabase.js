const sequelize = require("../config/database")

async function createDatabase (){
    try {
        await sequelize.sync();
        console.log("Banco de dados criado com sucesso")
    } catch (error) {
        console.log("erro da criacao do banco de dados")
        console.log(error)
    }
};

module.exports = createDatabase;